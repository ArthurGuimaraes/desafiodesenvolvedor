package desafiodesenvolvedor.src.main.java.com.arthur.cursomc.dto;


import java.io.Serializable;

import com.arthur.cursomc.domain.Usuarios;

public class HistoricoPrecoCombDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String preco;
	
	
	public HistoricoPrecoCombDTO() {
	}
	
	public HistoricoPrecoCombDTO(Usuarios obj) {
		id = obj.getId();
		preco = obj.getpreco();
		email = obj.getEmail();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getpreco() {
		return preco;
	}

	public void setpreco(String preco) {
		this.preco = preco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}

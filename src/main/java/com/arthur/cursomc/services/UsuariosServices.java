package com.arthur.cursomc.services;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arthur.cursomc.domain.Usuarios;
import com.arthur.cursomc.dto.UsuariosDTO;
import com.arthur.cursomc.repository.UsuariosRepository;
import com.arthur.cursomc.services.exception.ObjectNotFoundException;

@Service
public class UsuariosService {

	@Autowired
	private UsuariosRepository repo;

	public List<Usuarios> findAll() {
		return repo.findAll();
	}

	public Usuarios findById(String id) {
		Optional<Usuarios> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
	}
	
	public Usuarios insert(Usuarios obj) {
		return repo.insert(obj); 
	}
	
	public void delete(String id) {
		findById(id);
		repo.deleteById(id);
	}
	
	public Usuarios update(Usuarios obj) {
		Usuarios newObj = findById(obj.getId());
		updateData(newObj, obj);
		return repo.save(newObj);
	}
	
	private void updateData(Usuarios newObj, Usuarios obj) {
		newObj.setName(obj.getName());
		newObj.setEmail(obj.getEmail());
	}

	public Usuarios fromDTO(UsuariosDTO objDto) {
		return new Usuarios(objDto.getId(), objDto.getName(), objDto.getEmail());
	}
	
	
}

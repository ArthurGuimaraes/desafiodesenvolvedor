package com.arthur.cursomc.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;
//
//import com.pedroguarany.workshopmongo.dto.AuthorDTO;
//import com.pedroguarany.workshopmongo.dto.CommentDTO;

@Document
public class Post implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	private Date date;
	private String name;
	private String preco;
//	private AuthorDTO author;
//	private List<CommentDTO> comments = new ArrayList<>();
	
	public Post() {
	}

	public Post(String id, Date date, String name, String preco) {
		this.id = id;
		this.date = date;
		this.name = name;
		this.preco = preco;
//		this.author = author;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public String getpreco() {
		return preco;
	}

	public void setpreco(String preco) {
		this.preco = preco;
	}

//	public AuthorDTO getAuthor() {
//		return author;
//	}
//
//	public void setAuthor(AuthorDTO author) {
//		this.author = author;
//	}
//	
//	public List<CommentDTO> getComments() {
//		return comments;
//	}
//
//	public void setComments(List<CommentDTO> comments) {
//		this.comments = comments;
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}

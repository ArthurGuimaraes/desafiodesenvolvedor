package com.arthur.cursomc.resources;


import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.arthur.cursomc..domain.Post;
import com.arthur.cursomc.domain.usuarios;
import com.arthur.cursomc.dto.usuarioDTO;
import com.arthur.cursomc.services.usuarioService;

@RestController
@RequestMapping(value="/usuarios")
public class usuarioResource {

	@Autowired
	private usuarioService service;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<usuarioDTO>> findAll(){
		List<usuario> list = service.findAll();
		List<usuarioDTO> listDto = list.stream().map(x -> new usuarioDTO(x)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<usuarioDTO> findById(@PathVariable String id){
		usuario obj = service.findById(id);
		 return ResponseEntity.ok().body(new usuarioDTO(obj));
	}

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@RequestBody usuarioDTO objDto){
		usuario obj = service.fromDTO(objDto);
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable String id){
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody usuarioDTO objDto, @PathVariable String id){
		usuario obj = service.fromDTO(objDto);
		obj.setId(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
		}
	
	@RequestMapping(value="/{id}/posts", method=RequestMethod.GET)
	public ResponseEntity<List<Post>> findPosts(@PathVariable String id){
		usuario obj = service.findById(id);
		return ResponseEntity.ok().body(obj.getPosts());
	}
}


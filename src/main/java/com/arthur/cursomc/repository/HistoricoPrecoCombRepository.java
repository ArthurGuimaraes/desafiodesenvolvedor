package desafiodesenvolvedor.src.main.java.com.arthur.cursomc.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.pedroguarany.workshopmongo.domain.User;

@Repository
public interface UserRepository extends MongoRepository<User, String> {
}
